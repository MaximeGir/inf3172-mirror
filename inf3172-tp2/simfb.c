/**
 * Alexandre Leblanc 
 * LEBA03018507
 * & 
 * Maxime Girard
 * GIRM30058500
 * 17 avril 2014
 * 
 * Description -----------------------------------------------------------------
 * Le programme simfb reçoit dans le fichier < fichier > les processus sur 
 * lesquels l’ algorithme feedback ( d'écrit ci - dessous ) vont opérer. 
 * Les quantums des files F0 et F1 sont donnés respectivement avec les options
 *  -q et -Q . Le fichier < fichier > est un fichier ( texte ) d ’entiers 
 * non nuls séparés avec des blancs où chaque ligne d écrit un processus.
 *
 *Le fichier d'entré est suposé sans d'erreur et conforme au norme du programme.
 *
 *Le programme na pas de limite connus. Les structure sont conçu pour s'adapter 
 * dynamiquement.
 * Source ----------------------------------------------------------------------
 * 
 * Recursive Bubble Sort:
 * http://www.sal.ksu.edu/faculty/tim/CMST302/study_guide/topic7/bubble.html
 **/


#define _GNU_SOURCE

#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#define EOL 10

int atoi(const char *chaine);
int isdigit(int c);

typedef int Time;
typedef char * Line;

typedef struct{
    int cursor;
    int size_max;
    int size;
    int * array;
}Array;

typedef struct {
    int p;
    Time arrival;
    Array * cpues;
}Pid; 

 typedef struct node{
    struct node * next;
    Pid * pid;
}Node;


typedef struct{
    int len;
    int quantum_max;
    int quantum_cyle;
    Node * head_node;
    Node * ptr_node;
}Feedback;

/**
 * Crée une Struct qui contiendra une liste chainé
 * ainsi que les informations raltives à la file de CPU
 * @param quantum_zero
 * @return 
 */
Feedback  * feedback_init(int quantum_zero){
    Feedback * f = malloc(sizeof(Feedback));
    f->head_node = malloc(sizeof(Node));
    f->ptr_node  = malloc(sizeof(Node));
    f->ptr_node = f->head_node;
    f->head_node->pid = NULL;
    f->quantum_max = quantum_zero;

    return f;
}

static Pid * blank_pid = NULL;

/**
 * Crée un pid 0 
 * @return 
 */
Pid * get_blank_pid(){
 if(blank_pid == NULL)
 {
  blank_pid = malloc(sizeof(Pid));
 }
    blank_pid->cpues = malloc(sizeof(Array));
    blank_pid->cpues->size_max = 4;
    blank_pid->cpues->size = 0;
    blank_pid->cpues->cursor = 0;
    blank_pid->p = 0;
    blank_pid->cpues->array = (int*)calloc(blank_pid->cpues->size_max,sizeof(int));
  return blank_pid;
}
/**
 * Crée et instancie un noeud 
 * @param pid
 * @return 
 */
Node * node_factory(Pid * pid){
   
    Node * node = (Node*)malloc(sizeof(Node));
    node->pid = pid;
    node->next = NULL;
    return node;
}

/**
 * Ajoute a la fin de la liste chainé un noeud
 * @param f
 * @param node
 * @return 
 */
Node * append(Feedback ** f, Node * node){
    if((*f)->head_node == NULL){
        (*f) = feedback_init((*f)->quantum_max);
    }
    (*f)->ptr_node->next = node;
    (*f)->len++;
  
    return node;
}  
/**
 * Switch de place des nodes
 * @param l1
 * @param l2
 * @return 
 */
Node * list_switch( Node * l1, Node * l2 ){
    l1->next = l2->next;
    l2->next = l1;
    return l2;
}
/**
 * Pop de la list une node
 * @param f Feedback
 * @return Node * sinon NULL
 */
Node * pop(Feedback * f){
    if (f->head_node != NULL){
        Node * node = malloc(sizeof(Node));
        Node * next;
        memcpy(node,f->head_node,sizeof(Node));
        if (f->head_node->next != NULL){
            node->next = NULL;
            next = f->head_node ->next;
            f->head_node = next;
        }else{
            f->head_node = NULL;
        }
        f->len--;
        return node;
    }else {
        
        return NULL;
    }
    
}

/*
 * Size est un Bubble Sort pour les listes chainées
 * qui class dans ce ca si d'abord par temps d'arrivé puis par 
 * numero de pid
 * @param Node
 * @return 
 */

Node * sort( Node * node ){
    if( node == NULL ) return NULL;
    
    /* First push the larger items down */
    if( node->next !=NULL && node->pid->arrival > node->next->pid->arrival ){
        node = list_switch(node, node->next);
    }else{
        if (node->next !=NULL && node->pid->arrival == node->next->pid->arrival){
            if (node->next !=NULL && node->pid->p > node->next->pid->p){
                node = list_switch(node, node->next);
            }
        }
    }
    /* Always sort from second item on */
    node->next = sort(node->next);
    /* bubble smaller items up */
    
    if( node->next !=NULL && node->pid->arrival > node->next->pid->arrival ){
        node = list_switch(node, node->next);
        node->next = sort(node->next);
    }else{
        if (node->next !=NULL && node->pid->arrival == node->next->pid->arrival){
            if (node->next !=NULL && node->pid->p > node->next->pid->p){
                node = list_switch(node, node->next);
                node->next = sort(node->next);
            }
        }
    }
    return node;
}

/**
 * Exrtait convertie en int les char
 * @param string 
 * @param pos 
 * @return 
 */
int extract_int(char * string, int pos, int * ret_value){
    int new_int;
    int i = 0;
    int size = 4;
    char * old_int = calloc(size,sizeof(char));
    while (string[pos]!= ' ' && string[pos]!= EOL && string[pos]!= '\n' ){
        
        old_int[i] = string[pos]; 
        pos++;
        i++;
        if(i == size ){
            
            old_int = realloc(old_int,size * 2);
            size *= 2;      
        }
    }
    new_int  = atoi(old_int);
    free(old_int);
    ret_value[0] = pos;
    return new_int;
}
/**
 * Redimentionne un array dynamiquement
 * @param array
 */
void array_resize(Array * array){
    array->size_max *= 2;
    array->array = (int*)realloc(array->array,array->size_max * sizeof(int));
}
/**
 * Ajoute la fin du array une nouvelle valeur
 * @param array
 * @param p
 */
void array_add(Array * array,int p){
    
    if(array->size == (array->size_max - 1)){
        array_resize(array);
        array->array[array->size++] = p;
        
    }else{
        array->array[array->size++] = p;
    }
}
/**
 * Crée et remplis un pid 
 * @param line
 * @return une pid initialisé
 */
Pid * factory_pid(Line line){
    int i = 0;
    int j = 0;
    int ret_value[1];
    
    //need to be isolated
    Pid * newpid = malloc(sizeof(Pid));
    newpid->cpues = malloc(sizeof(Array));
    newpid->cpues->size_max = 4;
    newpid->cpues->size = 0;
    newpid->cpues->cursor = 0;
    newpid->cpues->array = (int*)calloc(newpid->cpues->size_max,sizeof(int));
    
    while (line[i] != EOL && line[i] != 0){
       
        if (line[i]!= ' '){
           
            switch (j){
                case 0: 
                    
                    newpid->p = extract_int(line, i, ret_value);
                    i = ret_value[0];
                break;
                
                case 1: 
                    newpid->arrival = extract_int(line, i, ret_value);
                    i = ret_value[0];
                break;
                    
                default:
                    array_add(newpid->cpues,extract_int(line, i, ret_value));
                    i = ret_value[0];
                break;
            }
            j++;
        }
        i++;
    }
    return newpid;
}
/**
 * Crée une lignes remplis des informations provenant de l'entré
 * @param fp
 * @param len
 * @return 
 */
Line factory_line(FILE * fp, size_t  *len ){
    Line ln = (Line)malloc(256);
    
    if ( fgets ( ln, 256, fp ) != NULL ){
        return ln;
    }else{
        return NULL;
    }
}

/**
 * Additione les ES ou les temps CPU
 * @param array
 * @return 
 */
Array * compress (Array  * array){
    int i =0; 
    int j =0;
    int value = 0;
    int new_value = 0;
    Array * new_array = malloc(sizeof(Array)); 
    new_array->array = calloc(array->size_max,sizeof(int)); 
    new_array->size = array->size;
    new_array->size_max = array->size_max;
    new_array->cursor =  array->cursor;
    while(i < array->size ){
        value = array->array[i];
        if (value >= 0){
            while (value >= 0 && i < array->size){
                
                new_value += value;
                value = array->array[++i];
            }
            if (new_value != 0 ){
                new_array->array[j++] = new_value;
                new_value = 0;
                value = 0;
            }
            
        }else{
            while (value < 0){
                
                new_value+= value;
                value = array->array[++i];
            }
            if (new_value != 0 ){
                new_array->array[j++] = new_value;
                new_value = 0;
                value = 0;
            }
        }   
    }
    free(array);
    new_array->size = j;
    return new_array;
}
/**
 * Deplace le dernier noeud ajouter en vers la tete
 * si f0 est vide
 * @param f
 */
void hack(Feedback * f){

    if (f->head_node != NULL && f->len >0 && f->head_node->pid == NULL){
        f->head_node = f->head_node->next; 
    }
}
/**
 * Execute les entrées sortie ou temps cpu 
 * @param pid_es
 * @param pid_array_es
 */
void execute_es_cpu(Pid * pid_es,int * pid_array_es){
    if (pid_array_es[pid_es->cpues->cursor] > 0 ){
        pid_array_es[pid_es->cpues->cursor]--;

    }else{
        pid_array_es[pid_es->cpues->cursor]++;
    }
}

void super_free(Node * node) {
    if(node != NULL && node->next !=NULL){
         super_free(node->next);
         free(node);
    }else{
        free(node);
    }
} 

int validate_nombre_argument(int nombre_arguments)
{
  if(nombre_arguments < 4)
  {
   print_erreur(ERR_PARMETRE_INCORRECT);
   return 0;
  }
  return 1;
}

/**
 * Fait la validation des entier
 * @param chaine_entier
 * @return 
 */
int est_entier(const char * chaine_entier)
{
  while(*chaine_entier)
  {
   if(!isdigit(*chaine_entier)) return 0;
   else ++chaine_entier;
  }
  return 1;
}
/**
 * Fait la validation des arguments
 * @param argv
 * @param quantum
 * @param index_quantum
 * @param index_next_quantum
 * @param selector
 * @return 
 */
int validate_quantum_args(const char ** argv, int *quantum, int index_quantum, int *index_next_quantum, char selector)
{
 if('-' == argv[index_quantum][0] && selector == argv[index_quantum][1])
 {
   if(strlen(argv[index_quantum]) == 2)
   { 
    if(est_entier(argv[index_quantum+1]))
    {
     *quantum = atoi(argv[index_quantum+1]);
     *index_next_quantum = index_quantum+2;
     return 1;
    }
   }
   else if(strlen(argv[index_quantum]) > 2)
   {
    char * entier_from_string = strcpy(malloc(sizeof(argv[index_quantum])),argv[index_quantum]);
    int length_entier = strlen(entier_from_string) - 2;
    char entier_naked[length_entier+1];
   
    memcpy( entier_naked, &entier_from_string[2], length_entier+1); 
    entier_naked[length_entier+1] = '\0';

    if(est_entier(entier_naked))
    {
      *quantum = atoi(entier_naked);
      *index_next_quantum = index_quantum+1;
      free(entier_from_string);
      return 1;
    }
   }  
 } 
 return 0;
}
/**
 * Execute pour tous es la fonction execute
 * @param es
 */
void for_each_execute(Feedback * es){
    Node * node = es->head_node;
    while (node != NULL){
        execute_es_cpu(node->pid,node->pid->cpues->array);
        if (node->next != NULL){
            node = node->next;
        }else{
            break;
        }
    }
}
/**
 * Crée un valeur absolue
 * @param n
 * @return une valeur positive
 */
int abs(int n){
    if (n>0 ){
        return n;
    }else{ 
        return -n;
    }
}
/**
 * Permet de determiner si le cpu doit entré en mode inactif 
 * @param feedback
 * @param f
 * @param f0
 * @param f1
 * @param fifo
 * @param es
 * @return Feedback acec ou sans pid 0
 */
Feedback * idler(Feedback * feedback, Feedback * f, Feedback * f0, Feedback * f1, Feedback * fifo ,Feedback * es){

    if(((f0->len) + (f1->len) + (fifo->len)) == 0){
        
            Node * n = NULL;
            blank_pid = get_blank_pid();
            n = node_factory(blank_pid);
            f->ptr_node = append(&f,n);
            hack(f);
            return f;
        
    }else if((f->len)>=2){
        if(f->head_node->pid->p == 0){
            pop(f);
            f->quantum_cyle = 0;
            return f;
        }
    }
    return f;
}
/**
 * Détermine si le pid devrait être un batch process
 * @param feedback
 * @param f0
 * @param f1
 * @param fifo
 * @param es
 * @return 1 si batch process sinon 0
 */
int batch_process(Feedback * feedback, Feedback * f0, Feedback * f1, Feedback * fifo ,Feedback * es){
    if(((feedback->len) + (f0->len) + (f1->len)) == 1 ){
        return 1;
    }else{
        return 0;
    }
} 
int main (int argc, char const *argv[]){           
  
    int quantum_zero, quantum_one, index_quantum_one, index_quantum_zero = 1, index_stub;

    if(validate_nombre_argument(argc) == 0) return EXIT_FAILURE; 	 
    if(!validate_quantum_args(argv, &quantum_zero, index_quantum_zero, &index_quantum_one, 'q'))
      print_erreur(ERR_AUTRE);   
    if(!validate_quantum_args(argv, &quantum_one, index_quantum_one, &index_stub, 'Q'))
      print_erreur(ERR_AUTRE);

    FILE * fp; 
    if((fp = fopen(argv[index_stub],"r")) == NULL)
    {
     print_erreur(ERR_OUVERTURE_FICHER);
     return EXIT_FAILURE;
    } 
 
    size_t * len = 0;
    Line ln;

    Feedback * feedback = feedback_init(quantum_zero);
    Pid * pid = NULL;

    Node * node;
    while((ln = factory_line(fp,len)) != NULL ){
      pid = factory_pid(ln);
      pid->cpues = compress(pid->cpues);

      node = node_factory(pid);
      feedback->ptr_node = append(&feedback,node);
      hack(feedback);

     }
    free(len);
    //free(node);  cause un erreur de memoire 

    feedback->ptr_node = feedback->head_node;
    feedback->head_node = sort(feedback->ptr_node);
    feedback->ptr_node = feedback->head_node;
    
    Feedback * f0 = feedback_init(quantum_zero);
    Feedback * f1 = feedback_init(quantum_one);
    Feedback * fifo = feedback_init(0);
    Feedback * es = feedback_init(0);

    Node * node2;
    Time time = 0;
    int cpu_used = 0;
    while (((feedback->len) + (f0->len) + (f1->len) + (fifo->len)) > 0){

        while (feedback->len > 0 && feedback->head_node->pid->arrival == time){
            if ((node2 = pop(feedback)) != NULL){
                f0->ptr_node = append(&f0, node2);
                hack(f0);
            }
        }
                       
        f0 = idler(feedback,f0,f0,f1,fifo,es);
        if(f0->len > 0 && f0->head_node->pid->p == 0 ){
            f0->ptr_node = f0->head_node;
        }
        
        if(es->len > 0 ){
          Pid * pid_es = es->head_node->pid;
          int * pid_array_es = pid_es->cpues->array;
          
          for_each_execute(es);
            if (pid_array_es[pid_es->cpues->cursor] == 0 ){
                if (pid_es->cpues->cursor < (pid_es->cpues->size -1)){
                    pid_es->cpues->cursor++;
                    if ((node2 = pop(es))!= NULL){
                         f0->ptr_node = append(&f0, node2);   
                         hack(f0);
                         fifo->quantum_cyle = 0;
                         f1->quantum_cyle = 0;
                        f0 = idler(feedback,f0,f0,f1,fifo,es);
                        if(f0->len > 0 && f0->head_node->pid->p == 0 ){
                            f0->ptr_node = f0->head_node;
                        }
                    }
                }else if (pid_es->cpues->cursor == (pid_es->cpues->size -1)){
                         pop(es);
                         fifo->quantum_cyle = 0;
                         f1->quantum_cyle = 0;
                }
            }
        }
        
        if(f0->len > 0){
            Pid * pid_f0 = f0->head_node->pid;
            int * pid_array = pid_f0->cpues->array;
            
            //si ce nest pas un ES
            if(pid_array[pid_f0->cpues->cursor] >= 0 ){
                
                if (f0->quantum_cyle == 0) {
                    if (batch_process(feedback,f0,f1,fifo,es) == 0){
                        if (pid_array[pid_f0->cpues->cursor] >= f0->quantum_max){
                            print_element(pid_f0->p,time,f0->quantum_max);
                        }else{
                            if (pid_f0->p == 0 ){
                                print_element(pid_f0->p,time,feedback->head_node->pid->arrival);
                            }else{
                                print_element(pid_f0->p,time,pid_array[pid_f0->cpues->cursor]);
                            }
                        }
                    //if batch process     
                    }else{
                        print_element(pid_f0->p,time,pid_array[pid_f0->cpues->cursor]);
                    }
                }
                
                execute_es_cpu(pid_f0,pid_array);
                f0->quantum_cyle++;
                cpu_used = 1;
                //if pid temps cpu == fin
                if (pid_f0->p !=0  && pid_array[pid_f0->cpues->cursor] == 0 ){
                    
                    //if temps array[position] < array.len
                    if (pid_f0->cpues->cursor < (pid_f0->cpues->size -1)){
                        pid_f0->cpues->cursor++;
                    }else if (pid_f0->cpues->cursor == (pid_f0->cpues->size -1)){
                        pop(f0);
                        f0->quantum_cyle = 0 ;
                    }
                }
                else if(batch_process(feedback,f0,f1,fifo,es)){
		    if (f0->quantum_cyle == f0->quantum_max){
                        f0->quantum_cyle =1;
                    }
                    
                }else if(pid_f0->p !=0 && f0->quantum_cyle == f0->quantum_max){
                    if ((node2 = pop(f0))!= NULL){
                        f1->ptr_node = append(&f1, node2);
                        f0->quantum_cyle = 0;
                        hack(f1);
                    }
                }
            }else{
                
                if ((node2 = pop(f0))!= NULL){
                    es->ptr_node = append(&es, node2);   
                    hack(es);
                    f0 = idler(feedback,f0,f0,f1,fifo,es);
                    hack(f0);
                    if(f0->len > 0 && f0->head_node->pid->p == 0 ){
                        f0->ptr_node = f0->head_node;
                        print_element(f0->head_node->pid->p,time,abs(pid_array[es->head_node->pid->cpues->cursor]));
                        f0->quantum_cyle = 1;
                    }
                }
            }
        }   
        
       	if(f1->len > 0 && f0->len == 0 && cpu_used == 0){
            Pid * pid_f1 = f1->head_node->pid;
            int * pid_f1_array = pid_f1->cpues->array;
            
            //si pid est positif
	    if(pid_f1_array[pid_f1->cpues->cursor] > 0 ){
                
                if ((es->len) < 1){
                    if (pid_f1->p != 0 && f1->quantum_cyle == 0 && pid_f1_array[pid_f1->cpues->cursor] >= f1->quantum_max){
                        print_element(pid_f1->p,time,f1->quantum_max);
                    }else if (pid_f1->p != 0 && f1->quantum_cyle == 0){
                        print_element(pid_f1->p,time,pid_f1_array[pid_f1->cpues->cursor]);
                    }
                }else{
                    if (f1->quantum_cyle == 0) {
                        print_element(pid_f1->p,time,abs(es->head_node->pid->cpues->array[es->head_node->pid->cpues->cursor]));
                    }
                
                }
                             
                execute_es_cpu(pid_f1,pid_f1_array);
                f1->quantum_cyle++;
                cpu_used = 1;
                //si cpu_cycle x terminer 
                if (pid_f1_array[pid_f1->cpues->cursor] == 0 ){
                    
                   
                    if (pid_f1->cpues->cursor < (pid_f1->cpues->size -1)){
                        pid_f1->cpues->cursor++;
                    }else if (pid_f1->cpues->cursor == (pid_f1->cpues->size -1)){
                        pop(f1);
                        f1->quantum_cyle = 0;
                    }
                }
                else if( f1->quantum_cyle == f1->quantum_max){
                    if ((node2 = pop(f1))!= NULL){
                        fifo->ptr_node = append(&fifo, node2);   
                        hack(fifo);
                        f1->quantum_cyle = 0;
                    }
                }else if (f1->len >= 2 && pid_f1->p == 0 ){
                    if ((node2 = pop(f1))!= NULL){
                    es->ptr_node = append(&es, node2);   
                    hack(es);
                    f0 = idler(feedback,f0,f0,f1,fifo,es);
                    f1->quantum_cyle = 0;
                        if(f0->len > 0 && f0->head_node->pid->p == 0 ){
                            f0->ptr_node = f0->head_node;
                        }
                    }
                }
            }else{
                if ((node2 = pop(f1))!= NULL){
                    es->ptr_node = append(&es, node2);   
                    hack(es);
                    f1->quantum_cyle = 0;
                    f0 = idler(feedback,f0,f0,f1,fifo,es);
                    if(f0->len > 0 && f0->head_node->pid->p == 0 ){
                        f0->ptr_node = f0->head_node;
                    }
                }
            }
        }

	if(fifo->len > 0 && f1->len  == 0 && f0->len == 0 && cpu_used == 0 ){
            Pid * pid_fifo = fifo->head_node->pid;
            int * pid_fifo_array = pid_fifo->cpues->array;
	    if(pid_fifo_array[pid_fifo->cpues->cursor] > 0 ){
                               
                if(es->len > 0){
                    if (fifo->quantum_cyle == 0){
                        print_element(pid_fifo->p,time,abs(es->head_node->pid->cpues->array[es->head_node->pid->cpues->cursor]));
                       
                    }
                }else{
                    if (fifo->quantum_cyle == 0){
                        print_element(pid_fifo->p,time,pid_fifo_array[pid_fifo->cpues->cursor]);
                    }
                }
                    
                              
                fifo->quantum_cyle++;
                execute_es_cpu(pid_fifo,pid_fifo_array);

                if (pid_fifo_array[pid_fifo->cpues->cursor] == 0 ){
                    if (pid_fifo->cpues->cursor < (pid_fifo->cpues->size -1)){
                        pid_fifo->cpues->cursor++;
                        
		    if ((node2 = pop(fifo))!= NULL){
                       fifo->quantum_cyle = 0;
                       f0->ptr_node = append(&f0, node2);   
                       hack(f0);
                      }
                    }else if (pid_fifo->cpues->cursor == (pid_fifo->cpues->size -1)){
                        pop(fifo);
                        fifo->quantum_cyle = 0;                    
                    }
                }
            }else{
                if ((node2 = pop(f1))!= NULL){
                    fifo->quantum_cyle = 0;
                    es->ptr_node = append(&es, node2);   
                    hack(es);
                    f0 = idler(feedback,f0,f0,f1,fifo,es);
                    if(f0->len > 0 && f0->head_node->pid->p == 0 ){
                        f0->ptr_node = f0->head_node;
                    }
                }
            }
        }
    cpu_used = 0;
    time++;
    }
    free(pid);
   
    super_free(f0->head_node);
    fclose (fp);

    exit(EXIT_SUCCESS);
    return 0;
}

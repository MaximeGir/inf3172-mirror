INF3172 : TP2
Par Maxime Girard & Alexandre Leblanc
AZIZ SALAH
Date limite de la remise : le 14-04-2014 avant 23h59

**Beaucoup de refactor est recquis.


1. Objectif pédagogique du travail
Algorithmes d’ordonnancement.
2. Description du problème
simfb -- Simule un algorithme d ’ ordonnancement de la famile des
algorithmes d ’ ordonnancement feedback
SYNOPSIS
--------
simfb -q < quantum > -Q < quantum > < fichier >
DESCRIPTION
-----------
Le programme simfb re ç oit dans le fichier < fichier > les processus sur
lesquels l ’ algorithme feedback ( d é crit ci - dessous ) vont op é rer . Les
quantums des files F0 et F1 sont donn é s respectivement avec les options
-q et -Q .
Le fichier < fichier > est un fichier ( texte ) d ’ entiers non nuls s é par é s
avec des blancs o ù chaque ligne d é crit un processus selon
l ’ interpr é tation suivante :
- Le premier entier de la ligne repr é sente le PID du processus . Ce PID
doit ê tre strictement positif . 0 est le PID du processus IDLE qui occupe
la CPU lorsque aucun processus n ’ est pr ê t .
- Le deuxi è me entier repr é sente le temps d ’ arriv é e du processus .
- Les entiers restants dans la ligne repr é sentent , s ’ ils sont
strictement positifs , des temps CPU et , s ’ ils sont strictement n é gatifs ,
des temps pour E/ S bloquantes . Un entier nul peut ê tre ignor é . Des temps
CPU successifs se regroupent en leur somme , et de m ê me pour les temps
d ’ E / S successifs .
Le format g é n é ral d ’ une ligne de < fichier > a la forme :
<pid > < temps_arriv ée > <cpu >
ou bien
<pid > < temps_arriv ée > <cpu > ( < cpu >| - < e_s >) * <cpu >
1TP2 AZIZ SALAH
o ù * veut dire 0 ou plus occurrences , + veut dire une ou plus
occurrences , et o ù tous les jetons <... > repr é sentent des entiers
strictement positifs .
L ’ algorithme feedback à simuler utilise trois files dont F0 , F1 g é ré es
avec des algorithmes tourniquet et F2 gé r é e par FIFO .
- Tout processus qui passe à l ’ é tat pr ê t entre dans la file F0 .
- La file F0 est plus prioritaire que F1 qui est plus prioritaire que F2 .
Ceci veut dire que les processus de F2 n ’ ont acc ès à la CPU que si F0 et
F1 sont vides et que les processus de F1 n ’ ont acc ès à la CPU que si F0
est vide .
- Un processus de F0 qui a pris la CPU durant un quantum de F0 rejoint F1 .
- Un processus de F1 qui a pris la CPU durant un quantum de F1 rejoint F2 .
- Tout processus interrompu à cause de l ’ arriv é e d ’ un processus plus
prioritaire reste à la t ê te de sa file pour ne pas perdre sa priorit é .
Ainsi un processus de F1 interrompu avant la fin de son quantum aura
droit à un quantum au complet à son prochain acc è s à la CPU .
- Un processus qui passe à l ’ é tat bloqu é quitte sa file ( et la CPU ) mais
rejoint F0 d è s qu ’ il passe à l ’ é tat pr ê t .
- Si au m ê me moment deux processus ou plus doivent entrer dans une m ê me
file , on favorise celui qui le plus petit PID .
3. Exemple d’exécution
Voici un exemple de ﬁchier des processus.
16 10 4 -2 23
2 5 0 11 10
La ligne «16 10 4 -2 23» s’interprète par : le processus de PID 16 arrive à l’instant 10 et demande un
traitement CPU de 4 unités de temps d’aﬃlé, suivi d’une opération d’E/S bloquante qui prend 2 unités
de temps et pour terminer il a besoin de 23 unités de temps CPU. Un processus est supposé être à l’état
bloqué durant toute la duré d’E/S.
Le programme simfben exécutant la spéciﬁcation reçue par le ﬁchier donné comme paramètre, il aﬃche
les résultats des algorithmes demandés avec les processus fournis. L’aﬃchage des résultats du programme
simfb doit ressembler à l’exemple suivant :
malt > cat exemple . txt
16 10 4 -2 23
2 5 0 11 10
malt > wc exemple . txt
2 10 31 exemple . txt
malt > # wc montre que le fichier contient deux lignes
malt > ./ simfb -q5 -Q 10 exemple . txt

malt > ls exempleXYZ . txt
ls : exempleXYZ . txt : No such file or directory
malt > ./ simfb -q5 -Q 10 exempleXYZ . txt
Erreur : erreur ouverture fichier
malt >
Le processus de PID 0 (IDLE) occupe la CPU lorsque aucun processus prêts n’est disponible. De plus, par
exemple «PID 16 | 10 | 4» veut dire le processus de PID 16 entre dans la CPU à l’instant 10 pour une
duré de 4 unités. Le format de l’aﬃchage donné ce dessus est à titre indicatif. Il faut considérer pour foutre
programme l’aﬃche que vous allez obtenir en utilisant les fonctions d’aﬃchage fournies.
4. Exigences non fonctionnelles
4.1. Gestion de l’allocation dynamique. Le programme doit s’adapter dynamiquement avec la taille
des données en utilisant l’allocation dynamique. Tout espace alloué dynamiquement doit être libéré dès
qu’il n’est plus requis.
4.2. Gestion des erreurs. Le programme suppose que les données du ﬁchier sont valides mais doit traiter
tous les autres cas d’erreurs possibles comme ﬁchier non trouvé par exemple. Seules les erreurs considérées
par la fonction print_erreur sont à traiter dans votre programme.
4.3. Composition du programme.
– «util.h» : déﬁnit les prototypes de fonctions utilitaires à utiliser pour l’aﬃchage. Le ﬁchier «util.h»
ne doit pas être modiﬁé.
– «util.c» : est une implémentation complète qui vous fournit les fonctions utilitaires pour l’aﬃchage.
Les fonctions fournies dans ce ﬁchier ne doivent pas être modiﬁées.
– «simfb.c» : comporte la fonction main et vos fonctions selon votre conception.
– La compilation de votre programme «simfb.c» se fait par la commande :
gcc - Wall - std = c99 simfb.c util . c -o simfb
– La compilation de votre programme ne devrait donner aucun avertissement (warnning).
– Votre ﬁchier «simfb.c» doit contenir les noms des auteurs et leurs codes permanents en commentaire.
– L’aﬃchage doit respecter les exemples dans les cas d’utilisation fournis en utilisant les fonction d’aﬃ-
chage fournies.
– Si votre programme «simfb.c» rencontre une erreur, il doit aﬃcher seulement un message d’erreur
avec print_erreur.
– Tout aﬃchage doit se faire seulement avec les fonctions fournies :
– print_element
– print_erreur
4.4. Portabilité du programme. Aﬁn de s’assurer de la probabilité de votre programme, celui-çi devrait
être compilé et testé sur le serveur malt.
3TP2 AZIZ SALAH
5. Ce que vous devez remettre
En équipe d’au plus deux personnes, un membre de l’équipe seulement remettra votre ﬁchier «simfb.c»
électroniquement dans Moodle en suivant le lien approprié. Pas de remise en double svp.
6. Pondération
– Tests de fonctionnement : 70%
– Exigences non fonctionnelles : 15%
– Structure du programme, commentaires, indentation ... : 15%
Remarques importantes :
– Un programme ne compilant pas se verra attribuer la note 0.
– Aucun programme reçu par courriel ne sera accepté. En cas de panne des serveurs, un délai supplé-
mentaire vous sera accordé sans pénalité de retard.
– Les règlements sur le plagiat seront strictement appliqués.
– 10% comme pénalité de retard par journée entamée. Après cinq jours, le travail ne sera pas accepté.
– La remise d’un ﬁchier "zippé" donne lieu à 10% de pénalité.
-*

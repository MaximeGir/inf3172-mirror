#ifndef UTIL_H
#define UTIL_H
#include <stdio.h>
#include <stdlib.h>


#define ERR_OUVERTURE_FICHER 0
#define ERR_PARMETRE_INCORRECT 1
#define ERR_AUTRE 2


/**
* À utiliser pour afficher les erreurs
*/
void print_erreur(int err);

/**
* À utiliser pour afficher une ligne exemple : "PID       16 :      10 -      4"
*/
void print_element(unsigned int pid, unsigned int temps , unsigned int duree);

#endif
